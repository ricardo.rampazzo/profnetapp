package com.fiap.profnet

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AnunciosActivityTestUi {

    @get:Rule
    val rule = ActivityTestRule<AnunciosActivity>(AnunciosActivity::class.java)

    @Test
    fun testOnClickQueroEnsinarButton() {
        onView(withId(R.id.queroEnsinarButton)).perform(click())
      }
}