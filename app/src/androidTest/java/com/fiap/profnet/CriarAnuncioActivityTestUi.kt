package com.fiap.profnet

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CriarAnuncioActivityTestUi {

    @get:Rule
    val rule = ActivityTestRule<CriarAnuncioActivity>(CriarAnuncioActivity::class.java)

    @Test
    fun testInputsFormCriarAnuncio() {
        onView(withId(R.id.inputNomeProfessor)).perform(click())
        onView(withId(R.id.inputTituloAnuncio)).perform(click())
        onView(withId(R.id.inputResumoAnuncio)).perform(closeSoftKeyboard()).perform(click())
        onView(withId(R.id.inputDescricaoAnuncio)).perform(closeSoftKeyboard()).perform(click())
        onView(withId(R.id.inputTelefoneContato)).perform(closeSoftKeyboard()).perform(click())
        onView(withId(R.id.inputValorHora)).perform(closeSoftKeyboard()).perform(click())
    }
}