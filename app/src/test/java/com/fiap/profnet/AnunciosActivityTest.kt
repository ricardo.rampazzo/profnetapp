import com.fiap.profnet.sender.Endpoint
import com.fiap.profnet.sender.NetworkUtils
import org.junit.Test
import retrofit2.Retrofit

class AnunciosActivityTest {

    @Test
    fun testRetrofitInstance() {
        val instance: Retrofit = NetworkUtils.getRetrofitInstance("http://192.168.15.73:8084/")

        assert(instance.baseUrl().url().toString() == "http://192.168.15.73:8084/")
    }

    @Test
    fun getAnunciosTest() {
        val retrofitClient = NetworkUtils
            .getRetrofitInstance("http://192.168.15.73:8084/")

        val endpoint = retrofitClient.create(Endpoint::class.java)

        var response = endpoint.getAnuncios().execute()

        assert(response.code() == 200)
    }
}