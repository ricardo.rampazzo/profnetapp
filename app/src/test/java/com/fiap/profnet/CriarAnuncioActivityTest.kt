import com.fiap.profnet.model.Anuncio
import com.fiap.profnet.sender.Endpoint
import com.fiap.profnet.sender.NetworkUtils
import org.junit.Test
import retrofit2.Retrofit

class CriarAnuncioActivityTest {

    @Test
    fun testRetrofitInstance() {
        val instance: Retrofit = NetworkUtils.getRetrofitInstance("http://192.168.15.73:8084/")

        assert(instance.baseUrl().url().toString() == "http://192.168.15.73:8084/")
    }

    @Test
    fun criarAnuncioTest() {
        val retrofitClient = NetworkUtils
            .getRetrofitInstance("http://192.168.15.73:8084/")

        val endpoint = retrofitClient.create(Endpoint::class.java)

        val body = Anuncio(
            tituloAnuncio = "123",
            resumoAnuncio = "123",
            textoAnuncio = "123",
            contato = "123",
            horaAula = 123,
            aulaOnline = "S",
            aulaPresencial = "S",
            nomeUsuario = "123",
            emailUsuario = "123",
            codigoCategoria = 4
        )

        var response = endpoint.criarAnuncio(body).execute()

        assert(response.code() == 201)
    }
}