package com.fiap.profnet.model

import com.google.gson.annotations.SerializedName

data class Anuncio(
    @SerializedName("titulo_anuncio")
    var tituloAnuncio: String,
    @SerializedName("resumo_anuncio")
    var resumoAnuncio: String,
    @SerializedName("texto_anuncio")
    var textoAnuncio: String,
    @SerializedName("contato")
    var contato: String,
    @SerializedName("hora_aula")
    var horaAula: Int,
    @SerializedName("aula_online")
    var aulaOnline: String,
    @SerializedName("aula_presencial")
    var aulaPresencial: String,
    @SerializedName("nome_usuario")
    var nomeUsuario: String,
    @SerializedName("email_usuario")
    var emailUsuario: String,
    @SerializedName("codigo_categoria")
    var codigoCategoria: Int,
)
