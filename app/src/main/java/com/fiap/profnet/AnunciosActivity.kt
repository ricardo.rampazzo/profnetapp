package com.fiap.profnet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fiap.profnet.model.Data
import com.fiap.profnet.sender.Endpoint
import com.fiap.profnet.sender.NetworkUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AnunciosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anuncios)

        getData()
        clickListener()
    }

    override fun onStart() {
        super.onStart()
        getData()
    }

    fun getData() {

        val linearLayout = findViewById<LinearLayout>(R.id.anunciosLayout)
        val retrofitClient = NetworkUtils
            .getRetrofitInstance("http://192.168.15.73:8084/")

        val endpoint = retrofitClient.create(Endpoint::class.java)
        val callback = endpoint.getAnuncios()

        callback.enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Toast.makeText(baseContext, t.message, Toast.LENGTH_SHORT).show()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                response.body()!!.anuncios.forEach {

                    var titulo = TextView(this@AnunciosActivity);
                    titulo.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    titulo.setPadding(2,20,0,5);
                    titulo.textSize = 20F
                    titulo.setTypeface(null, Typeface.BOLD);
                    titulo.setTextColor(Color.parseColor("#FF03A9F4"))
                    titulo.text = it.tituloAnuncio

                    var resumo = TextView(this@AnunciosActivity);
                    resumo.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    resumo.setPadding(2,5,0,5);
                    resumo.setTextColor(Color.parseColor("#FF000000"))
                    resumo.text = it.resumoAnuncio

                    var nomeUsuario = TextView(this@AnunciosActivity);
                    nomeUsuario.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    nomeUsuario.setPadding(2,5,0,5);
                    nomeUsuario.setTextColor(Color.parseColor("#FF000000"))
                    nomeUsuario.textSize = 16F
                    nomeUsuario.setTypeface(null, Typeface.BOLD);
                    nomeUsuario.text = it.nomeUsuario

                    var horaAula = TextView(this@AnunciosActivity);
                    horaAula.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    horaAula.setPadding(2,5,0,5);
                    horaAula.setTextColor(Color.parseColor("#FF000000"))
                    horaAula.setTypeface(null, Typeface.BOLD);
                    horaAula.text = "R$ " + it.horaAula.toString() + " / H"

                    var contato = TextView(this@AnunciosActivity);
                    contato.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    contato.setPadding(2,5,0,30);
                    contato.setTextColor(Color.parseColor("#FF4CAF50"))
                    contato.setTypeface(null, Typeface.BOLD);
                    contato.text = it.contato

                    val line = View(this@AnunciosActivity)
                    line.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 1
                    )
                    line.setBackgroundResource(R.color.gray_400)

                    linearLayout.addView(titulo)
                    linearLayout.addView(resumo)
                    linearLayout.addView(nomeUsuario)
                    linearLayout.addView(horaAula)
                    linearLayout.addView(contato)
                    linearLayout.addView(line)
                }
            }


        })

    }

    private fun sendToCriarAnuncioActivity() {
        var intent = Intent(this@AnunciosActivity, CriarAnuncioActivity::class.java)
        startActivity(intent)
    }

    private fun clickListener() {
        val queroEnsinarButton = findViewById(R.id.queroEnsinarButton) as Button

        queroEnsinarButton.setOnClickListener {
            sendToCriarAnuncioActivity()
        }
    }

}