package com.fiap.profnet.sender

import com.fiap.profnet.model.Anuncio
import com.fiap.profnet.model.Data
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Endpoint {

    @GET("anuncios")
    fun getAnuncios() : Call<Data>

    @POST("anuncios")
    fun criarAnuncio(@Body body: Anuncio): Call<Anuncio>
}