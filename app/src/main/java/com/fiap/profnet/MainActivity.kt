package com.fiap.profnet

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.fiap.profnet.databinding.ActivityMainBinding
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        val view = binding.root

        setContentView(view)

        auth = FirebaseAuth.getInstance()

        binding.botaoEntrar.setOnClickListener {

            var usuario = binding.editTextTextUsuario.text.toString()
            var senha = binding.editTextTextSenha.text.toString()

            auth.signInWithEmailAndPassword(usuario, senha)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "signInWithCustomToken:success")
                        Toast.makeText(
                            baseContext, "Login efetuado com sucesso!",
                            Toast.LENGTH_SHORT
                        ).show()
                        sendToAnunciosActivity()
                    } else {
                        Log.w(TAG, "signInWithCustomToken:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Erro ao realizar login.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    public override fun onStart() {
        super.onStart()
    }

    fun sendToAnunciosActivity() {
        var intent = Intent(this@MainActivity, AnunciosActivity::class.java)
        startActivity(intent)
    }
}