package com.fiap.profnet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fiap.profnet.model.Anuncio
import com.fiap.profnet.sender.Endpoint
import com.fiap.profnet.sender.NetworkUtils
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CriarAnuncioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_criar_anuncio)

        clickListener()
    }

    fun sendData() {

        var inputNomeProfessor = findViewById(R.id.inputNomeProfessor) as EditText
        var inputTituloAnuncio = findViewById(R.id.inputTituloAnuncio) as EditText
        var inputResumoAnuncio = findViewById(R.id.inputResumoAnuncio) as TextInputLayout
        var inputDescricaoAnuncio = findViewById(R.id.inputDescricaoAnuncio) as TextInputLayout
        var inputTelefoneContato = findViewById(R.id.inputTelefoneContato) as EditText
        var inputValorHora = findViewById(R.id.inputValorHora) as EditText
        var emailUsuario = FirebaseAuth.getInstance().currentUser

        val body = Anuncio(
            tituloAnuncio = inputTituloAnuncio.text.toString(),
            resumoAnuncio = inputResumoAnuncio.editText?.text.toString(),
            textoAnuncio = inputDescricaoAnuncio.editText?.text.toString(),
            contato = inputTelefoneContato.text.toString(),
            horaAula = Integer.parseInt(inputValorHora.text.toString()),
            aulaOnline = "S",
            aulaPresencial = "S",
            nomeUsuario = inputNomeProfessor.text.toString(),
            emailUsuario = emailUsuario?.email.toString(),
            codigoCategoria = 4
        )

        val retrofitClient = NetworkUtils
            .getRetrofitInstance("http://192.168.15.73:8084/")

        val endpoint = retrofitClient.create(Endpoint::class.java)
        val callback = endpoint.criarAnuncio(body)

        callback.enqueue(object : Callback<Anuncio> {
            override fun onFailure(call: Call<Anuncio>, t: Throwable) {
                Toast.makeText(baseContext, t.message, Toast.LENGTH_SHORT).show()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<Anuncio>, response: Response<Anuncio>) {
                Toast.makeText(
                    baseContext, "Anuncio criado com sucesso!",
                    Toast.LENGTH_SHORT
                ).show()
                sendToAnunciosActivity()
            }

        })
    }

    private fun sendToAnunciosActivity() {
        var intent = Intent(this@CriarAnuncioActivity, AnunciosActivity::class.java)
        startActivity(intent)
    }

    private fun clickListener() {
        val queroEnsinarButton = findViewById(R.id.enviarAnuncioButton) as Button

        queroEnsinarButton.setOnClickListener {
            sendData()
        }
    }
}